# Komputer Store App

Computer store assignment solution.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Download project and run index.html.

## Usage

Run index.html file.

## Maintainers

[Oliver Dahlqvist (@OliverDahlqvist)](https://gitlab.com/OliverDahlqvist)

## Contributing

Not open for contributing.

## License

MIT