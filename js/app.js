//Bank elements.
const pBankBalance = document.getElementById("balance");
const loanButton = document.getElementById("loanButton");

// Work elements.
const pWorkBalance = document.getElementById("pay");
const workButton = document.getElementById("workButton");
const bankButton = document.getElementById("bankButton");
const repayLoanButton = document.getElementById("repayLoanButton")

// Selection elements.
const list = document.getElementById("list");
const itemTemplate = document.getElementById("itemTemplate");

// Laptop elements.
const laptopFeatures = document.getElementById("features");
const pLaptopName = document.getElementById("laptopName");
const pLaptopDescription = document.getElementById("laptopDescription");
const pLaptopImage = document.getElementById("laptopImage");
const pPrice = document.getElementById("price");
const buyButton = document.getElementById("buyButton");

// Variables.
let salary = 100;
let laptops = [];
let workBalance = 0;
let bankBalance = 0;
let currentLoan = 0;
let selectedLaptop;

// Event listeners.
bankButton.addEventListener("click", () => {DepositIntoBank(0.1)});
repayLoanButton.addEventListener("click", () => {DepositIntoBank(1)});
workButton.addEventListener("click", Work);
loanButton.addEventListener("click", TakeLoan);
buyButton.addEventListener("click", PurchaseLaptop);
list.addEventListener("change", UpdateSelectedLaptop);

// Formatters.
const formatSek = new Intl.NumberFormat('en-SE', {style: 'currency', currency: 'SEK'})
const formatNok = new Intl.NumberFormat('en-SE', {style: 'currency', currency: 'NOK'})
let currentFormat = formatSek;

/**
 * Returns a formatted currency with specified formatter.
 * @param {string}  line 
 * @param {NumberFormat} formatter 
 * @returns {string} Formatted string.
 */
function FormatCurrency(line, formatter){
    return formatter.format(line);
}

// Updates the work balance element.
function UpdateWorkBalance(){
    pWorkBalance.innerText = `\nPay ${FormatCurrency(workBalance, currentFormat)}`;
}

// Updates the bank balance element.
function UpdateBankBalance(){
    pBankBalance.innerText = `Balance ${FormatCurrency(bankBalance, currentFormat)}\nLoan ${FormatCurrency(currentLoan, currentFormat)}`;
}

// Checks if there is a loan, shows/hides repay loan button.
function UpdateRepayLoanButton(){
    repayLoanButton.style.display = currentLoan > 0 ? "block" : "none";
}

// Populates laptop information in respective document elements.
function UpdateSelectedLaptop(){
    if(laptops.length <= 0) return;

    selectedLaptop = laptops[list.selectedIndex];
    let specs = selectedLaptop.specs;
    let result = "";
    for (let i = 0; i < specs.length; i++) {
        result += specs[i] + "\n";
    }
    laptopFeatures.innerText = result;
    pLaptopName.innerText = selectedLaptop.title;
    pLaptopDescription.innerText = selectedLaptop.description;
    pLaptopImage.src = 'https://noroff-komputer-store-api.herokuapp.com/' + selectedLaptop.image;
    pPrice.innerText = FormatCurrency(selectedLaptop.price, currentFormat);
}

// Purchase laptop and displays alert, subtracts the cost from bank balance.
function PurchaseLaptop(){
    let price = selectedLaptop.price;
    if(bankBalance < price){
        let missingBalance = price - bankBalance;
        window.alert(`Insufficient bank balance: ${FormatCurrency(bankBalance, currentFormat)}.\nMissing: ${FormatCurrency(missingBalance, currentFormat)}.`);
        return;
    }
    bankBalance -= price;
    window.alert(`Successfully purchased ${selectedLaptop.title} for:  ${FormatCurrency(price, currentFormat)}.\nNew balance: ${FormatCurrency(bankBalance, currentFormat)}`);
    UpdateBankBalance();
}

// Generates money based on salary.
function Work(){
    workBalance += salary;
    UpdateWorkBalance();
}

/**
 * Deposits and deducts work balance to bank balance.
 * @param {float} deductionAmount Percentage amount to deduct from work balance.
 */
function DepositIntoBank(deductionAmount){    
    if(currentLoan > 0){
        let deduction = (workBalance * deductionAmount);
        if(currentLoan < deduction){
            deduction = currentLoan;
        }
        if(deductionAmount < 1){
            currentLoan += deduction;
        }
        else{
            currentLoan -= deduction;
        }
        bankBalance -= deduction;
    }
    bankBalance += workBalance;
    workBalance = 0;
    UpdateBankBalance();
    UpdateWorkBalance();
    UpdateRepayLoanButton();
}

// Opens prompt and verifies if the loan is acceptable.
function TakeLoan(){
    if(currentLoan > 0){
        window.alert(`Loan denied due to a loan already existing, remaining loan amount: ${FormatCurrency(currentLoan, currentFormat)}`);
        return;
    }
    let maxLoan = bankBalance * 2;
    let loanAmount = parseFloat(prompt(`Enter loan amount in sek.\nMax loan amount: ${FormatCurrency(maxLoan, currentFormat)}`));
    
    if(isNaN(loanAmount) || loanAmount <= 0){
        window.alert(`Input a number between ${1} and  ${maxLoan}`);
        return;
    }

    if(bankBalance * 2 < loanAmount){
        window.alert(`Loan (${FormatCurrency(loanAmount, currentFormat)}) denied due to insufficient funds. Maximum loan amount is: ${FormatCurrency(maxLoan, currentFormat)}`);
    }
    else{
        bankBalance += loanAmount;
        currentLoan += loanAmount;
        UpdateBankBalance();
        window.alert(`${(FormatCurrency(loanAmount, currentFormat))} transfered to bank balance.\nCurrent loan: ${FormatCurrency(currentLoan, currentFormat)}`);
        UpdateRepayLoanButton();
    }
}

/**
 * Fetches laptop-json from API.
 * @returns {Object[]} Laptops as json.
 */
async function FetchLaptops(){
    try{
        const loadedLaptops = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers');
        laptops = await loadedLaptops.json();
        LoadLaptops(laptops);
        UpdateSelectedLaptop();
        return laptops;
    }
    catch(error){
        console.log(error);
    }
}

/** 
 * Loads the fetched data into the dropdown menu.
 * @param {Object[]} laptops Array of computer-json.
 */
function LoadLaptops(laptops){
    for (let i = 0; i < laptops.length; i++) {
        let item = itemTemplate.content.cloneNode(true).querySelector('option');
        item.value = laptops[i].id;
        item = list.appendChild(item);
        item.childNodes[0].innerText = laptops[i].title;
    }
}

// Fetch all laptops from API.
FetchLaptops();